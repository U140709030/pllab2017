use strict;
use warnings;

my $str= 12345;
my $rev_str=reverse($str);
print "Original String :" . $str . "\n";
print "length of String :" . length($str) . "\n";
print "Reversed String : ". $rev_str."\n";
print 50000+$str."\n\n";

print "reverse complement\n";
my $dna = "ATGCAttT";
my $rev_comp = reverse($dna);
$rev_comp =~ tr/AaTtGgCc/TtAaCcGg/;
print "$dna\n";
print "$rev_comp\n\n";

print "array of gene names\n";
my @genes = ("HOXB1", "ALPK1", "TP53");
my $size = scalar @genes;
print "A list of $size genes: @genes\n";
@genes = reverse @genes;
print "Reversed list of $size genes: @genes\n";
@genes = sort @genes;
print "Sorted list of $size genes: @genes\n\n";


print "Appending to an array\n";
my @genes2 = ("HOXB1", "ALPK1", "TP53");
my $size2 = scalar @genes2;
print "Our Array Firstly $size2 genes: @genes2\n";
push @genes2, "ZZZ3";
print "There are now $size2 genes: @genes2\n";
push @genes2, ("EGF", "EFGR");
$size2 = scalar @genes2;
print "There are now $size2 genes: @genes2\n\n";

print "Removing items from end of array\n";
my @genes3 = ("HOXB1", "ALPK1", "TP53", "EGF");
my $size3 = scalar @genes3;
print "A list of $size3 genes: @genes3\n";
pop @genes3;
$size3 = scalar @genes3;
print "There are now $size3 genes: @genes3\n";
my $gene3 = pop @genes3;
$size3 = scalar @genes3;
print "There are now $size3 genes: @genes3\n";
print "There gene removed was $gene3\n\n";


print "Removing items from front of array\n";
my @genes4 = ("HOXB1", "ALPK1", "TP53", "EGF");
my $size4 = scalar @genes4;
print "A list of $size4 genes: @genes4\n";
shift @genes4;
$size4 = scalar @genes4;
print "There are now $size4 genes: @genes4\n";
my $gene4 = shift @genes4;
$size4 = scalar @genes4;
print "There are now $size4 genes: @genes4\n";
print "There gene removed was $gene4\n\n";

print "Adding items from front of array\n";
my @genes6 = ("HOXB1", "ALPK1", "TP53", "EGF","AAA");
my $size6 = scalar @genes6;
print "A list of $size6 genes: @genes6\n";
unshift @genes6,"AAAAA";
$size6 = scalar @genes6;
print "There are now $size4 genes: @genes4\n";
my $gene6 = unshift @genes6,"GGGGG";
$size6 = scalar @genes6;
print "There are now $size6 genes: @genes6\n";
print "There gene removed was $gene6\n\n";


my @genes5 = ("HOXB1", "ALPK1", "TP53");
print "There are now  @genes5\n";
my $i=0;
while ($i<scalar @genes5) {
    #my $gene5 = shift @genes5;,
    
    print "Processing gene $genes5[$i]\n";
    $i++;
    
# put processing code here
}
print "\n\n";


my @genes7 = ("HOXB1", "ALPK1", "TP53");
while ("@genes7") {
    my $gene7 = shift @genes7;
    print "Processing gene $gene7\n";
# put processing code here
}

my $size7 = scalar @genes7;
print "There are now $size7 genes in the list: @genes7\n";
print "\n\n";

print "for each loop to process all items from a list\n";
my @genes8 = ("HOXB1", "ALPK1", "TP53");

foreach my $gene8 (@genes8) {
    print "Processing gene $gene8\n";
    # put processing code here
    

}
my $size8 = scalar @genes8;
print "There are still $size8 genes in the list: @genes8\n";
print "\n\n";


print "another for loop to process a list\n";
my @genes9 = ("HOXB1", "ALPK1", "TP53");
my $size9 = scalar @genes9;
for (my $l = 0; $l < $size; $l++) {
    my $gene9 = $genes9[$l];
    print "Processing gene $gene9\n";
# put processing code here
}
$size9 = scalar @genes9;
print "There are still $size9 genes in the list: @genes9\n";

print "\njoin  ecxplample";
my $string =join(",",@genes9);
print "$string\n";

print "\n\n";

print "converting string to array\n";
my $dna10 = "ATGCATTT";
my @bases10 = split "", $dna10;
print "dna = $dna10\n";
my $size10 = scalar @bases10;
print "The list of $size10 bases: @bases10\n\n";





print "Split explample\n";
my $string2= "Ali;VELİ;ZEYNEP;Ayşe;Deniz";
my @names2 = split ";",$string2;
print "@names2\n";

